package main

import (
	"log"

	"bitbucket.org/meersau/tftp"
)

func main() {
	srv, err := tftp.NewServer("udp4", ":5555", "troot", "", "", true, true)
	if err != nil {
		log.Fatalf("can't create new server: %v", err)
	}

	srv.AddACL("127.0.0.1", true, true, "upload", "upload")
	a1 := &tftp.ACL{true, true, "", ""}
	srv.AddACLStruct("127.0.0.2", a1)
	log.Fatal(srv.Serve())
}
